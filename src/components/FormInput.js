import React from 'react';
import {Text, View, TextInput} from 'react-native';

export default class FormInput extends React.PureComponent {
  render() {
    const {singleInput, children, setResult, type, value, isNumber} =
      this.props;
    return (
      <View>
        <Text style={[styles.inputHeader, singleInput ? styles.dNone : false]}>
          {children}
        </Text>
        <TextInput
          style={styles.inputField}
          onChangeText={text => setResult(text, type)}
          value={value}
          {...(isNumber
            ? {keyboardType: 'numeric'}
            : {keyboardType: 'default'})}
        />
      </View>
    );
  }
}

const styles = {
  inputHeader: {
    marginLeft: 12,
    marginBottom: 10,
    fontSize: 20,
  },
  inputField: {
    height: 50,
    margin: 12,
    textAlign: 'center',
    borderWidth: 1,
    borderColor: 'black',
  },
  dNone: {
    display: 'none',
  },
};
